module gitlab.com/golight/migrator

go 1.19

require (
	github.com/DATA-DOG/go-sqlmock v1.5.2
	github.com/Masterminds/squirrel v1.5.4
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.2.0
	github.com/valyala/quicktemplate v1.7.0
	gitlab.com/golight/dao v1.0.8
	gitlab.com/golight/scanner v1.0.6
	golang.org/x/sync v0.5.0
)

require (
	github.com/friendsofgo/errors v0.9.2 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/volatiletech/inflect v0.0.1 // indirect
	github.com/volatiletech/null/v8 v8.1.2 // indirect
	github.com/volatiletech/randomize v0.0.1 // indirect
	github.com/volatiletech/strmangle v0.0.1 // indirect
	gitlab.com/golight/entity v1.0.0 // indirect
	golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7 // indirect
)
